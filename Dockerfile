FROM node:8.13
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
# Avaa portti
EXPOSE 3000

# Käynnistä serveri
CMD "npm" "start"